//可以在在代码中获取用户自定义环境变量的TypeScript智能提示
interface ImportMetaEnv {
  readonly VITE_BASE_API: string
  // 更多环境变量 ...
}

interface ImportMeta {
  readonly env: ImportMetaEnv
}

