import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

//引入path模块
import {resolve} from "path"

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],

  //配置环境变量文件的路径
  envDir: "./src", 
  
  //读取除VITE开头的其他前缀
  envPrefix: ["VITE_","APP_","NODE_"],

  //配置别名
  resolve:{
    alias:{
      '@': resolve(__dirname,"src"),
      '~': resolve(__dirname,"src")
    }
  },

  //白屏解决
  // base:"/home"

  //跨域代理设置
  server: {
    port: 8002,
    //允许跨域
    cors: true,
    //遇到api开头的，自动替换为target对应的地址请求
    proxy: {  
      '/api': {  //注意：这里的api要和request.ts中的baseURL保持一致
        target: "http://xxxx.com",
        changeOrigin: true
      }
    }

  },
  
})
