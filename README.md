# 项目说明
本项目是VUE3 + TS + Pinia的应用基础框架，可以clone到本地直接使用。

该项目下有2个分支，分别为mobile和master。
- master：默认为pc端应用开发。
- mobile：为移动端应用开发。


# 组件列表
```shell

# 状态存储
pnpm i pinia -S

# 状态存储持久化(可选)
pnpm i pinia-plugin-persistedstate

# 路由
pnpm i vue-router

# 安装内置node，用于path别名设置
pnpm i -D @types/node

# API
pnpm i @vueuse/core -S
# 网络请求
pnpm i axios -S

# 动画(可选)
pnpm i animate.css

# UI
pnpm i element-plus
pnpm i @element-plus/icons-vue

# 样式
pnpm i less
pnpm i -D sass

# (可选)
pnpm i moment

# 原子化样式(可选)
pnpm i unocss 

```