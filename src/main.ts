import { createApp } from 'vue'
import '@/assets/css/reset.css'
import App from './App.vue'
import router from "@/router"


import {createPinia} from "pinia"
import {createPersistedState} from "pinia-plugin-persistedstate"

const app = createApp(App)

const pinia = createPinia()
app.use(pinia)

//pinia的持久化全局配置
pinia.use(createPersistedState({
  storage: localStorage,
  key: id => `__persisted__${id}`,  //指定key的设置
  auto: true
}))

app.use(router)
app.mount('#app')
