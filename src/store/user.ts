import { defineStore } from "pinia"

export const useUserStore = defineStore({
  id: "user",
  state: () => {
    return {
      token: "",
      userInfo: {}
    }
  },

  //类似于计算属性
  getters:{

  },

  //方法
  actions: {
    setUserInfo(data: any) {
      this.token = data.token
      this.userInfo = data.user_info
    }
  }
})