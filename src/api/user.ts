import request from "@/utils/request"

//get
export const getUser = (data:any) =>{
  return request({
    url:"user/list",
    method:"get",
    params:data
  })
}

//post
export const editUser = (data:any) =>{
  return request({
    url:"user/edit",
    method:"post",
    params:data
  })
}