//createRouter：用于创建vue-router实例对象
//RouteRecordRaw: 用于规范，路由规则，增加路由对象类型限制
//createWebHistory

import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router"


//创建路由规则
const routes: Array<RouteRecordRaw> = [
  {
    path: "/home",
    name: "home",
    component: () => import("@/page/public/home/index.vue"),
    meta:{
      title: "首页",
      noLogin: true, //无需登录即可浏览
    }
  },
  {
    path: "/",
    redirect: "/home"
  },
  {
    path: "/login",
    component: () => import("@/page/public/login/index.vue"),
    children: [],
    meta: {
      noLogin: true, //无需登录即可浏览
    }
  },
  {
    path: "/notAllow",
    name: "notAllow",
    component: () => import("@/page/public/sys/notAllow.vue"),
    children: [],
    meta: {
      noLogin: true, //无需登录即可浏览
    }
  }
]

//创建路由实例
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: routes
})


//路由导航守卫
import { useUserStore } from "@/store/user"
router.beforeEach((to, from, next) => {
  // const user = useUserStore()
  // const isLogin = user.token ? true : false
  // if (isLogin) {
  //   next()
  // } else {
  //   // router.push({ path: "/" })
  //   console.log("未登录")
  // }
  next()

})
export default router