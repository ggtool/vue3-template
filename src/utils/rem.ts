//移动端 rem适配

//基准大小
const baseSize = 37.5
//适配方案
const setRem = ()=> {
  const scale = document.documentElement.clientWidth / 750
  document.documentElement.style.fontSize = baseSize * Math.min(scale,1) + 'px'
}

//初始化
setRem()

//改变窗口大小的时候，重新设置一下rem
window.onresize = ()=> setRem

export default baseSize