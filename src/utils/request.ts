import { useUserStore } from "@/store/user"
import axios from "axios"
import {ElMessage} from "element-plus"

const service = axios.create({
  baseURL: "/api", //请求的基础路径设置
  timeout: 10000
})

//请求拦截器
service.interceptors.request.use((config) => {
  const userStore = useUserStore()
  if(userStore.token){
    config.headers.token = userStore.token
  }
  return config
},(error) => {
  return Promise.reject(error)
})

//响应拦截器
service.interceptors.response.use((response) => {
  return response.data
},(error) =>{
  // let status = error.response.status 
  ElMessage.error(error.message)
  return Promise.reject(new Error(error.message))
})


export default service